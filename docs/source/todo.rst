woohoo pDNS GUI's ToDo list or whishlist
========================================


Some things I am considering to add
-----------------------------------

- make table sortable (JavaScript)
- support for basic authentication
- filtering by ``rrtype``
- searching for "first seen in last 24 hours" or similar


Some things I am looking for from the community
-----------------------------------------------

- Init scripts (especially for Linux)
- Reverse proxy configurations for other webservers than lighttpd
- General tips and tricks to run Python web applications (e.g. logging)
