Support
=======

If you need to get help with woohoo pDNS GUI feel free to open an issue on
Gitlab and I will do my best to help out.

Please understand however that this currently is a private project run in my
free time and that I can only spend as much time on it as I can.

Be assured: I will come back to you; just maybe not right now?
