Contributing
============

To build woohoo pDNS GUI (and the documentation), three additional dependencies
exist::

    pip-tools
    nose
    sphinx-rtd-theme

You can easily install them using the following pip command in your development
environment::

    $ pip install -r dev-requirements.txt

To build the documentation after cloning the repository, run the following
command in the ``woohoo_pdns_gui/docs`` directory::

    $ make html

Note: do *not* run::

    $ sphinx-quickstart

To run the (inexisting) tests, issue the following command::

    $ python setup.py test



Managing dependencies
---------------------

Following the advice of people with (much) more experience in that field
(namely `Vincent Driessen`_ and `Hynek Schlawack`_) woohoo pDNS pins its
dependencies.

The tool used is `pip-tools`_, for the runtime dependencies in
`Hash-Checking Mode`_, and here's how.

.. _`pip-tools`: https://pypi.org/project/pip-tools/
.. _`Hash-Checking Mode`: https://pip.pypa.io/en/stable/reference/pip_install/#hash-checking-mode


Runtime dependencies
********************

Dependencies required to *run* woohoo pDNS GUI are listed in the
``install_requires`` variable in ``setup.py``::

    setup(
        <snip>
        install_requires = [
            "flask",
            "gunicorn",
            <snap>
        ]
    )

If you want to add a new (run time) dependency for woohoo pDNS GUI, this is the
place to do so.


Build dependencies
******************

Dependencies required to *develop* woohoo pDNS GUI are listed in the
``dev-requirements.in`` file::

    pip-tools
    ...



Using ``pip-tools`` for woohoo pDNS GUI
***************************************

To *generate a requirements.txt file* (i.e. a ``requirements.txt`` file that
listing the runtime dependencies), run the following command (you have
``pip-tools`` installed, right?)::

    $ pip-compile --allow-unsafe --generate-hashes

This will *overwrite* the current requirements.txt file with the most recent
version available on PiPI for every package and will *add new dependencies*
also.

To check if there are newer versions of dependencies available in PyPI, use the
following command::

    $ pip-compile --allow-unsafe --generate-hashes --upgrade

This will *overwrite* the current requirements.txt file with the most recent
version available on PiPI for every package. It will *not* add new dependencies
though.

Note: ``pip-compile`` has a ``dry-run`` command line switch.

To *generate the ``dev-requirements.txt`` file* (i.e. a file listing the build
dependencies), run the following command::

    $ pip-compile --allow-unsafe --output-file=dev-requirements.txt dev-requirements.in

This will *overwrite* the current ``dev-requirements.txt`` file with the most
recent version available on PiPI for every package and will *add new
dependencies* also.

To check if there are newer versions of build dependencies available in PyPI,
use the following command::

    $ pip-compile --upgrade --allow-unsafe --output-file=dev-requirements.txt dev-requirements.in

This will *overwrite* the current ``dev-requirements.txt`` file with the most
recent version available on PiPI for every package. It will *not* add new
dependencies though.

References:

- `Pin Your Packages`_
- `Better Package Management`_
- `Python Application Dependency Management in 2018`_
- `pip-tools (GitHub)`_

.. _`Vincent Driessen`: https://nvie.com/about/
.. _`Hynek Schlawack`: https://hynek.me/
.. _`Pin Your Packages`: https://nvie.com/posts/pin-your-packages/
.. _`Better Package Management`: https://nvie.com/posts/better-package-management/
.. _`Python Application Dependency Management in 2018`: https://hynek.me/articles/python-app-deps-2018/
.. _`pip-tools (GitHub)`: https://github.com/jazzband/pip-tools
