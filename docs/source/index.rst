.. woohoo pDNS GUI documentation master file, created by
   sphinx-quickstart on Thu Aug 15 21:42:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to woohoo pDNS GUI's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   todo
   contribute
   support
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


What woohoo pDNS GUI is
=======================

woohoo pDNS GUI is a Python 3 web application (Flask) to display passive DNS
data that is provided by a server that complies to the `Common Passive DNS
Output Format`_.

This is what it looks like:

.. image:: _static/screenshot_01.png
   :width: 800px
   :height: 526px
   :align: center
   :alt: screenshot

There is a `Database component`_ that can be used but must be installed
separately.

If you want to know in more detail what (a) passive DNS (data(base)) is,
`the FAQ on the Farsight Security website`_ is a valid resource to read up on
the topic.

.. _Common Passive DNS Output Format: http://tools.ietf.org/html/draft-dulaunoy-dnsop-passive-dns-cof-01
.. _Database component: https://gitlab.com/scherand/woohoo-pdns
.. _the FAQ on the Farsight Security website: https://www.farsightsecurity.com/technical/passive-dns/passive-dns-faq/#q11
